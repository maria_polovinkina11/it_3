﻿namespace IT_3
{
    partial class Poligarmonic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Ap1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_phase1_p = new System.Windows.Forms.TextBox();
            this.textBox_phase2_p = new System.Windows.Forms.TextBox();
            this.textBox_f1_p = new System.Windows.Forms.TextBox();
            this.textBox_phase3_p = new System.Windows.Forms.TextBox();
            this.textBox_f3_p = new System.Windows.Forms.TextBox();
            this.textBox_f2_p = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Ap3 = new System.Windows.Forms.TextBox();
            this.textBox_Ap2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_Ap1
            // 
            this.textBox_Ap1.Location = new System.Drawing.Point(62, 58);
            this.textBox_Ap1.Name = "textBox_Ap1";
            this.textBox_Ap1.Size = new System.Drawing.Size(85, 22);
            this.textBox_Ap1.TabIndex = 0;
            this.textBox_Ap1.Text = "1";
            this.textBox_Ap1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBox_phase1_p);
            this.panel1.Controls.Add(this.textBox_phase2_p);
            this.panel1.Controls.Add(this.textBox_f1_p);
            this.panel1.Controls.Add(this.textBox_phase3_p);
            this.panel1.Controls.Add(this.textBox_f3_p);
            this.panel1.Controls.Add(this.textBox_f2_p);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox_Ap3);
            this.panel1.Controls.Add(this.textBox_Ap2);
            this.panel1.Controls.Add(this.textBox_Ap1);
            this.panel1.Location = new System.Drawing.Point(34, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(571, 259);
            this.panel1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(449, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "Фаза 3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(449, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Фаза 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(242, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Частота 3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(242, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Частота 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Амплитуда 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Амплитуда 2";
            // 
            // textBox_phase1_p
            // 
            this.textBox_phase1_p.Location = new System.Drawing.Point(443, 58);
            this.textBox_phase1_p.Name = "textBox_phase1_p";
            this.textBox_phase1_p.Size = new System.Drawing.Size(85, 22);
            this.textBox_phase1_p.TabIndex = 13;
            this.textBox_phase1_p.Text = "0";
            this.textBox_phase1_p.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_phase2_p
            // 
            this.textBox_phase2_p.Location = new System.Drawing.Point(443, 125);
            this.textBox_phase2_p.Name = "textBox_phase2_p";
            this.textBox_phase2_p.Size = new System.Drawing.Size(85, 22);
            this.textBox_phase2_p.TabIndex = 12;
            this.textBox_phase2_p.Text = "0";
            this.textBox_phase2_p.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_f1_p
            // 
            this.textBox_f1_p.Location = new System.Drawing.Point(245, 58);
            this.textBox_f1_p.Name = "textBox_f1_p";
            this.textBox_f1_p.Size = new System.Drawing.Size(85, 22);
            this.textBox_f1_p.TabIndex = 11;
            this.textBox_f1_p.Text = "0,03";
            this.textBox_f1_p.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_phase3_p
            // 
            this.textBox_phase3_p.Location = new System.Drawing.Point(443, 197);
            this.textBox_phase3_p.Name = "textBox_phase3_p";
            this.textBox_phase3_p.Size = new System.Drawing.Size(85, 22);
            this.textBox_phase3_p.TabIndex = 10;
            this.textBox_phase3_p.Text = "0";
            this.textBox_phase3_p.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_f3_p
            // 
            this.textBox_f3_p.Location = new System.Drawing.Point(243, 197);
            this.textBox_f3_p.Name = "textBox_f3_p";
            this.textBox_f3_p.Size = new System.Drawing.Size(85, 22);
            this.textBox_f3_p.TabIndex = 8;
            this.textBox_f3_p.Text = "0,01";
            this.textBox_f3_p.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_f2_p
            // 
            this.textBox_f2_p.Location = new System.Drawing.Point(245, 125);
            this.textBox_f2_p.Name = "textBox_f2_p";
            this.textBox_f2_p.Size = new System.Drawing.Size(85, 22);
            this.textBox_f2_p.TabIndex = 7;
            this.textBox_f2_p.Text = "0,02";
            this.textBox_f2_p.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(242, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Частота 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(449, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Фаза 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Амплитуда 1";
            // 
            // textBox_Ap3
            // 
            this.textBox_Ap3.Location = new System.Drawing.Point(62, 197);
            this.textBox_Ap3.Name = "textBox_Ap3";
            this.textBox_Ap3.Size = new System.Drawing.Size(85, 22);
            this.textBox_Ap3.TabIndex = 2;
            this.textBox_Ap3.Text = "3";
            this.textBox_Ap3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_Ap2
            // 
            this.textBox_Ap2.Location = new System.Drawing.Point(62, 125);
            this.textBox_Ap2.Name = "textBox_Ap2";
            this.textBox_Ap2.Size = new System.Drawing.Size(85, 22);
            this.textBox_Ap2.TabIndex = 1;
            this.textBox_Ap2.Text = "2";
            this.textBox_Ap2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Параметры полигармонического сигнала";
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(529, 293);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(76, 32);
            this.button_OK.TabIndex = 3;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(434, 293);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(76, 32);
            this.button_cancel.TabIndex = 4;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // Poligarmonic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 337);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "Poligarmonic";
            this.Text = "Poligarmonic";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBox_Ap1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textBox_phase1_p;
        public System.Windows.Forms.TextBox textBox_phase2_p;
        public System.Windows.Forms.TextBox textBox_f1_p;
        public System.Windows.Forms.TextBox textBox_phase3_p;
        public System.Windows.Forms.TextBox textBox_f3_p;
        public System.Windows.Forms.TextBox textBox_f2_p;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBox_Ap3;
        public System.Windows.Forms.TextBox textBox_Ap2;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Button button_cancel;
    }
}