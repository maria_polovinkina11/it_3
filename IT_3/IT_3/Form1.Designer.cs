﻿namespace IT_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series27 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button_parametrs = new System.Windows.Forms.Button();
            this.ChartSignal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.checkBox_Ps = new System.Windows.Forms.CheckBox();
            this.checkBox_Gauss = new System.Windows.Forms.CheckBox();
            this.checkBox_ZPs = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_len = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chart_SobstvZnach = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button_FindSob = new System.Windows.Forms.Button();
            this.textBox_P = new System.Windows.Forms.TextBox();
            this.textBox_M = new System.Windows.Forms.TextBox();
            this.textBox_N = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_num = new System.Windows.Forms.TextBox();
            this.chart_SobFunc = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox_chSL = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSignal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SobstvZnach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SobFunc)).BeginInit();
            this.SuspendLayout();
            // 
            // button_parametrs
            // 
            this.button_parametrs.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_parametrs.Location = new System.Drawing.Point(1278, 47);
            this.button_parametrs.Name = "button_parametrs";
            this.button_parametrs.Size = new System.Drawing.Size(145, 87);
            this.button_parametrs.TabIndex = 0;
            this.button_parametrs.Text = "Задать параметры моделируемого сигнала(сигналов) и отрисовка";
            this.button_parametrs.UseVisualStyleBackColor = true;
            this.button_parametrs.Click += new System.EventHandler(this.button_parametrs_Click);
            // 
            // ChartSignal
            // 
            this.ChartSignal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            chartArea7.Name = "ChartArea1";
            this.ChartSignal.ChartAreas.Add(chartArea7);
            legend5.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend5.Name = "Legend1";
            this.ChartSignal.Legends.Add(legend5);
            this.ChartSignal.Location = new System.Drawing.Point(12, 28);
            this.ChartSignal.Name = "ChartSignal";
            this.ChartSignal.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series19.ChartArea = "ChartArea1";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series19.Color = System.Drawing.Color.Blue;
            series19.Legend = "Legend1";
            series19.LegendText = "Полигармонический сигнал";
            series19.MarkerColor = System.Drawing.Color.Blue;
            series19.MarkerSize = 7;
            series19.Name = "Series1";
            series20.ChartArea = "ChartArea1";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series20.Color = System.Drawing.Color.DarkTurquoise;
            series20.Legend = "Legend1";
            series20.LegendText = "Гауссовы купола";
            series20.Name = "Series2";
            series21.ChartArea = "ChartArea1";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series21.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            series21.Legend = "Legend1";
            series21.LegendText = "Экспонециально-затухающий полигармонический сигнал";
            series21.Name = "Series3";
            this.ChartSignal.Series.Add(series19);
            this.ChartSignal.Series.Add(series20);
            this.ChartSignal.Series.Add(series21);
            this.ChartSignal.Size = new System.Drawing.Size(968, 328);
            this.ChartSignal.TabIndex = 1;
            this.ChartSignal.Text = "chart1";
            // 
            // checkBox_Ps
            // 
            this.checkBox_Ps.AutoSize = true;
            this.checkBox_Ps.Location = new System.Drawing.Point(1015, 68);
            this.checkBox_Ps.Name = "checkBox_Ps";
            this.checkBox_Ps.Size = new System.Drawing.Size(224, 21);
            this.checkBox_Ps.TabIndex = 2;
            this.checkBox_Ps.Text = "1.Полигармонический сигнал";
            this.checkBox_Ps.UseVisualStyleBackColor = true;
            // 
            // checkBox_Gauss
            // 
            this.checkBox_Gauss.AutoSize = true;
            this.checkBox_Gauss.Location = new System.Drawing.Point(1015, 113);
            this.checkBox_Gauss.Name = "checkBox_Gauss";
            this.checkBox_Gauss.Size = new System.Drawing.Size(154, 21);
            this.checkBox_Gauss.TabIndex = 3;
            this.checkBox_Gauss.Text = "2.Гауссовы купола";
            this.checkBox_Gauss.UseVisualStyleBackColor = true;
            // 
            // checkBox_ZPs
            // 
            this.checkBox_ZPs.AutoSize = true;
            this.checkBox_ZPs.Location = new System.Drawing.Point(1015, 164);
            this.checkBox_ZPs.Name = "checkBox_ZPs";
            this.checkBox_ZPs.Size = new System.Drawing.Size(420, 21);
            this.checkBox_ZPs.TabIndex = 4;
            this.checkBox_ZPs.Text = "3.Экспонециально затухающий полигармонический сигнал";
            this.checkBox_ZPs.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1001, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Выберите тип моделируемого сигнала";
            // 
            // textBox_len
            // 
            this.textBox_len.Location = new System.Drawing.Point(1186, 199);
            this.textBox_len.Name = "textBox_len";
            this.textBox_len.Size = new System.Drawing.Size(121, 22);
            this.textBox_len.TabIndex = 6;
            this.textBox_len.Text = "512";
            this.textBox_len.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1012, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Количество отсчётов";
            // 
            // chart_SobstvZnach
            // 
            this.chart_SobstvZnach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            chartArea8.Name = "ChartArea1";
            this.chart_SobstvZnach.ChartAreas.Add(chartArea8);
            legend6.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend6.Name = "Legend1";
            this.chart_SobstvZnach.Legends.Add(legend6);
            this.chart_SobstvZnach.Location = new System.Drawing.Point(12, 380);
            this.chart_SobstvZnach.Name = "chart_SobstvZnach";
            this.chart_SobstvZnach.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series22.ChartArea = "ChartArea1";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series22.Color = System.Drawing.Color.Blue;
            series22.Legend = "Legend1";
            series22.LegendText = "Полигармонический сигнал";
            series22.MarkerColor = System.Drawing.Color.Blue;
            series22.MarkerSize = 7;
            series22.Name = "Series1";
            series22.YValuesPerPoint = 2;
            series23.ChartArea = "ChartArea1";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series23.Color = System.Drawing.Color.DarkTurquoise;
            series23.Legend = "Legend1";
            series23.LegendText = "Гауссовы купола";
            series23.Name = "Series2";
            series24.ChartArea = "ChartArea1";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series24.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            series24.Legend = "Legend1";
            series24.LegendText = "Экспонециально-затухающий полигармонический сигнал";
            series24.Name = "Series3";
            this.chart_SobstvZnach.Series.Add(series22);
            this.chart_SobstvZnach.Series.Add(series23);
            this.chart_SobstvZnach.Series.Add(series24);
            this.chart_SobstvZnach.Size = new System.Drawing.Size(968, 328);
            this.chart_SobstvZnach.TabIndex = 8;
            this.chart_SobstvZnach.Text = "chart_SobZnach";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(363, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(230, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Графики моделирумеых сигналов";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(393, 359);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Собственные значения";
            // 
            // button_FindSob
            // 
            this.button_FindSob.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_FindSob.Location = new System.Drawing.Point(1327, 275);
            this.button_FindSob.Name = "button_FindSob";
            this.button_FindSob.Size = new System.Drawing.Size(145, 95);
            this.button_FindSob.TabIndex = 11;
            this.button_FindSob.Text = "Найти собственные значения и собственный функции";
            this.button_FindSob.UseVisualStyleBackColor = true;
            this.button_FindSob.Click += new System.EventHandler(this.button_FindSob_Click);
            // 
            // textBox_P
            // 
            this.textBox_P.Location = new System.Drawing.Point(1004, 275);
            this.textBox_P.Name = "textBox_P";
            this.textBox_P.Size = new System.Drawing.Size(121, 22);
            this.textBox_P.TabIndex = 12;
            this.textBox_P.Text = "50";
            this.textBox_P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_M
            // 
            this.textBox_M.Location = new System.Drawing.Point(1156, 341);
            this.textBox_M.Name = "textBox_M";
            this.textBox_M.Size = new System.Drawing.Size(121, 22);
            this.textBox_M.TabIndex = 13;
            this.textBox_M.Text = "5";
            this.textBox_M.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_N
            // 
            this.textBox_N.Location = new System.Drawing.Point(1156, 275);
            this.textBox_N.Name = "textBox_N";
            this.textBox_N.Size = new System.Drawing.Size(121, 22);
            this.textBox_N.TabIndex = 14;
            this.textBox_N.Text = "5";
            this.textBox_N.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1012, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Порядок АКП";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1202, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "N";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1201, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "M";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1001, 319);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Номер соб.функции";
            // 
            // textBox_num
            // 
            this.textBox_num.Location = new System.Drawing.Point(1004, 339);
            this.textBox_num.Name = "textBox_num";
            this.textBox_num.Size = new System.Drawing.Size(121, 22);
            this.textBox_num.TabIndex = 19;
            this.textBox_num.Text = "0";
            this.textBox_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chart_SobFunc
            // 
            this.chart_SobFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            chartArea9.Name = "ChartArea1";
            this.chart_SobFunc.ChartAreas.Add(chartArea9);
            this.chart_SobFunc.Location = new System.Drawing.Point(989, 386);
            this.chart_SobFunc.Name = "chart_SobFunc";
            this.chart_SobFunc.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series25.ChartArea = "ChartArea1";
            series25.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series25.Color = System.Drawing.Color.Blue;
            series25.LegendText = "Полигармонический сигнал";
            series25.MarkerColor = System.Drawing.Color.Blue;
            series25.MarkerSize = 7;
            series25.Name = "Series1";
            series26.ChartArea = "ChartArea1";
            series26.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series26.Color = System.Drawing.Color.DarkTurquoise;
            series26.LegendText = "Гауссовы купола";
            series26.Name = "Series2";
            series27.ChartArea = "ChartArea1";
            series27.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series27.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            series27.LegendText = "Экспонециально-затухающий полигармонический сигнал";
            series27.Name = "Series3";
            this.chart_SobFunc.Series.Add(series25);
            this.chart_SobFunc.Series.Add(series26);
            this.chart_SobFunc.Series.Add(series27);
            this.chart_SobFunc.Size = new System.Drawing.Size(460, 264);
            this.chart_SobFunc.TabIndex = 20;
            this.chart_SobFunc.Text = "chart_SobFunc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1144, 366);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Собственные функции";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.Location = new System.Drawing.Point(1315, 656);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 57);
            this.button1.TabIndex = 25;
            this.button1.Text = "Информация о СЛУ и ее решении";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox_chSL
            // 
            this.checkBox_chSL.AutoSize = true;
            this.checkBox_chSL.Location = new System.Drawing.Point(1045, 675);
            this.checkBox_chSL.Name = "checkBox_chSL";
            this.checkBox_chSL.Size = new System.Drawing.Size(138, 21);
            this.checkBox_chSL.TabIndex = 26;
            this.checkBox_chSL.Text = "Частный случай";
            this.checkBox_chSL.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1484, 734);
            this.Controls.Add(this.checkBox_chSL);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chart_SobFunc);
            this.Controls.Add(this.textBox_num);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_N);
            this.Controls.Add(this.textBox_M);
            this.Controls.Add(this.textBox_P);
            this.Controls.Add(this.button_FindSob);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chart_SobstvZnach);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_len);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_ZPs);
            this.Controls.Add(this.checkBox_Gauss);
            this.Controls.Add(this.checkBox_Ps);
            this.Controls.Add(this.ChartSignal);
            this.Controls.Add(this.button_parametrs);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ChartSignal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SobstvZnach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SobFunc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_parametrs;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartSignal;
        private System.Windows.Forms.CheckBox checkBox_Ps;
        private System.Windows.Forms.CheckBox checkBox_Gauss;
        private System.Windows.Forms.CheckBox checkBox_ZPs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_len;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_SobstvZnach;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_FindSob;
        private System.Windows.Forms.TextBox textBox_P;
        private System.Windows.Forms.TextBox textBox_M;
        private System.Windows.Forms.TextBox textBox_N;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_num;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_SobFunc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.CheckBox checkBox_chSL;
    }
}

