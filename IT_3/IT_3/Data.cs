﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IT_3
{
    class Data
    {
        private UInt16 Data_InSize;
        private Double[] Data_Ampl;
        private Double[] Data_f;
        private Double[] Data_phase;

        private Double[] Data_Disp;
        private Int16[] Data_Pos;

        // Конструктор:
        public Data()
        {
            Data_InSize = 3;
            Data_Ampl = new Double[Data_InSize];
            Data_f = new Double[Data_InSize];
            Data_phase = new Double[Data_InSize];
            Data_Disp = new Double[Data_InSize];
            Data_Pos = new Int16[Data_InSize];

        }
        public Double[] Get_Ampl(double a1, double a2, double a3)
        {
            Data_Ampl[0] = a1;
            Data_Ampl[1] = a2;
            Data_Ampl[2] = a3;
            return Data_Ampl;

        }
        public Int16[] Get_Pos(Int16 x1, Int16 x2, Int16 x3)
        {
            Data_Pos[0] = x1;
            Data_Pos[1] = x2;
            Data_Pos[2] = x3;
            return Data_Pos;

        }
        public Double[] Get_Disp(double d1, double d2, double d3)
        {
            Data_Disp[0] = d1;
            Data_Disp[1] = d2;
            Data_Disp[2] = d3;
            return Data_Disp;
        }

        public  Double[] Get_f(double f1, double f2, double f3)
        {
            Data_f[0] = f1;
            Data_f[1] = f2;
            Data_f[2] = f3;
            return Data_f;
        }

        public Double[] Get_phase(double phase1, double phase2, double phase3)
        {
            Data_phase[0] = phase1;
            Data_phase[1] = phase2;
            Data_phase[2] = phase3;
            return Data_phase;
        }

        public Double[] Gen_Poligarmonic_Signal(Int16 Length, Double a1, Double a2, Double a3,
                                                Double f1, Double f2, Double f3,
                                                Double phase1,Double phase2, Double phase3)
        {
            Get_Ampl(a1, a2, a3);
            Get_f(f1, f2, f3);
            Get_phase(phase1, phase2, phase3);
            Double[] Signal = new Double[Length];
            for (UInt16 i = 0; i < Length; i++)
            {
               
                for (UInt16 k = 0; k < Data_InSize; k++)
                {
                    if (Data_Ampl[k] > 0.0)
                    {
                        Signal[i] += Data_Ampl[k] * Math.Sin(2 * Math.PI * Data_f[k] * i + Data_phase[k]);
                    }
                }
            }
            return Signal;
        }


            public Double[] Gen_Gauss_Signal(Int16 Length, Double a1, Double a2, Double a3,
                                             Double d1, Double d2, Double d3, 
                                             Int16 x1, Int16 x2, Int16 x3)
        {
            Get_Ampl(a1, a2, a3);
            Get_Pos(x1, x2, x3);
            Get_Disp(d1, d2, d3);
            Double[] Signal = new Double[Length];
            for (UInt16 i = 0; i < Length; i++)
            {
                Signal[i] = 0.0;
                Double Pow = 0.0;
                for (UInt16 k = 0; k < Data_InSize; k++)
                {
                    if (Data_Ampl[k] > 0.0)
                    {
                        Pow = (Double)(i - Data_Pos[k]) / Data_Disp[k];
                        Pow = -Pow * Pow;
                        Signal[i] += Data_Ampl[k] * Math.Exp(Pow);
                    }
                }
            }
            return Signal;
        }

        public Double[] Gen_ExpZ_Signal(Int16 Length, Double a1, Double a2, Double a3,
                                        Double f1, Double f2, Double f3,
                                        Double phase1, Double phase2, Double phase3)
        {
            Get_Ampl(a1, a2, a3);
            Get_f(f1, f2, f3);
            Get_phase(phase1, phase2, phase3);
            Double[] Signal = new Double[Length];
            for (UInt16 i = 0; i < Length; i++)
            {
                
                for (UInt16 k = 0; k < Data_InSize; k++)
                {
                    if (Data_Ampl[k] > 0.0)
                    {
                        Signal[i] += Math.Exp(-Data_Ampl[k]*i) * Math.Sin(2 * Math.PI * Data_f[k] * i + Data_phase[k]);
                    }
                }
            }
            return Signal;
        }







    }
}
