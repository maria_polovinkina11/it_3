﻿namespace IT_3
{
    partial class Result
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_cancel = new System.Windows.Forms.Button();
            this.textBox_det = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_IshodMatr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_Nevayzky = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_MatrLS = new System.Windows.Forms.TextBox();
            this.textBox_MprS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_transpMatrL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_SZ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_PsObr = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_Neizv = new System.Windows.Forms.TextBox();
            this.textBox_svobod = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(699, 666);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(142, 30);
            this.button_cancel.TabIndex = 1;
            this.button_cancel.Text = "Выход";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // textBox_det
            // 
            this.textBox_det.Location = new System.Drawing.Point(699, 462);
            this.textBox_det.Name = "textBox_det";
            this.textBox_det.Size = new System.Drawing.Size(121, 22);
            this.textBox_det.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(706, 433);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Детерминант";
            // 
            // textBox_IshodMatr
            // 
            this.textBox_IshodMatr.Location = new System.Drawing.Point(15, 24);
            this.textBox_IshodMatr.Multiline = true;
            this.textBox_IshodMatr.Name = "textBox_IshodMatr";
            this.textBox_IshodMatr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_IshodMatr.Size = new System.Drawing.Size(248, 176);
            this.textBox_IshodMatr.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Псевдобратная матрица";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(727, 507);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 17);
            this.label10.TabIndex = 25;
            this.label10.Text = "Невязки";
            // 
            // textBox_Nevayzky
            // 
            this.textBox_Nevayzky.Location = new System.Drawing.Point(699, 542);
            this.textBox_Nevayzky.Name = "textBox_Nevayzky";
            this.textBox_Nevayzky.Size = new System.Drawing.Size(121, 22);
            this.textBox_Nevayzky.TabIndex = 24;
            this.textBox_Nevayzky.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 455);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(269, 17);
            this.label3.TabIndex = 26;
            this.label3.Text = "Матрица правых собственных векторов";
            // 
            // textBox_MatrLS
            // 
            this.textBox_MatrLS.Location = new System.Drawing.Point(15, 236);
            this.textBox_MatrLS.Multiline = true;
            this.textBox_MatrLS.Name = "textBox_MatrLS";
            this.textBox_MatrLS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_MatrLS.Size = new System.Drawing.Size(248, 176);
            this.textBox_MatrLS.TabIndex = 27;
            // 
            // textBox_MprS
            // 
            this.textBox_MprS.Location = new System.Drawing.Point(15, 475);
            this.textBox_MprS.Multiline = true;
            this.textBox_MprS.Name = "textBox_MprS";
            this.textBox_MprS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_MprS.Size = new System.Drawing.Size(292, 176);
            this.textBox_MprS.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(261, 17);
            this.label4.TabIndex = 29;
            this.label4.Text = "Матрица левых собственных векторов";
            // 
            // textBox_transpMatrL
            // 
            this.textBox_transpMatrL.Location = new System.Drawing.Point(363, 236);
            this.textBox_transpMatrL.Multiline = true;
            this.textBox_transpMatrL.Name = "textBox_transpMatrL";
            this.textBox_transpMatrL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_transpMatrL.Size = new System.Drawing.Size(248, 176);
            this.textBox_transpMatrL.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(306, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(341, 17);
            this.label5.TabIndex = 31;
            this.label5.Text = "Транспонированная матрица левых соб. векторов";
            // 
            // textBox_SZ
            // 
            this.textBox_SZ.Location = new System.Drawing.Point(363, 475);
            this.textBox_SZ.Multiline = true;
            this.textBox_SZ.Name = "textBox_SZ";
            this.textBox_SZ.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_SZ.Size = new System.Drawing.Size(248, 176);
            this.textBox_SZ.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(360, 455);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(256, 17);
            this.label6.TabIndex = 33;
            this.label6.Text = "Матрица соб.значений в степени(-1.)";
            // 
            // textBox_PsObr
            // 
            this.textBox_PsObr.Location = new System.Drawing.Point(363, 24);
            this.textBox_PsObr.Multiline = true;
            this.textBox_PsObr.Name = "textBox_PsObr";
            this.textBox_PsObr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_PsObr.Size = new System.Drawing.Size(248, 176);
            this.textBox_PsObr.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(360, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(256, 17);
            this.label7.TabIndex = 35;
            this.label7.Text = "Матрица соб.значений в степени(-1.)";
            // 
            // textBox_Neizv
            // 
            this.textBox_Neizv.Location = new System.Drawing.Point(687, 24);
            this.textBox_Neizv.Multiline = true;
            this.textBox_Neizv.Name = "textBox_Neizv";
            this.textBox_Neizv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_Neizv.Size = new System.Drawing.Size(154, 176);
            this.textBox_Neizv.TabIndex = 36;
            // 
            // textBox_svobod
            // 
            this.textBox_svobod.Location = new System.Drawing.Point(687, 236);
            this.textBox_svobod.Multiline = true;
            this.textBox_svobod.Name = "textBox_svobod";
            this.textBox_svobod.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_svobod.Size = new System.Drawing.Size(154, 176);
            this.textBox_svobod.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(674, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 17);
            this.label8.TabIndex = 38;
            this.label8.Text = "Вектор свободных членов";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(674, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(194, 17);
            this.label9.TabIndex = 39;
            this.label9.Text = "Вектор неизвестных членов";
            // 
            // Result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 734);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox_svobod);
            this.Controls.Add(this.textBox_Neizv);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox_PsObr);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox_SZ);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_transpMatrL);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_MprS);
            this.Controls.Add(this.textBox_MatrLS);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox_Nevayzky);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_IshodMatr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_det);
            this.Controls.Add(this.button_cancel);
            this.Name = "Result";
            this.Text = "Result";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBox_det;
        public System.Windows.Forms.TextBox textBox_IshodMatr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox textBox_Nevayzky;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textBox_MatrLS;
        public System.Windows.Forms.TextBox textBox_MprS;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textBox_transpMatrL;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textBox_SZ;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox textBox_PsObr;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox textBox_Neizv;
        public System.Windows.Forms.TextBox textBox_svobod;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}