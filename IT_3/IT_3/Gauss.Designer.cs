﻿namespace IT_3
{
    partial class Gauss
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_D1 = new System.Windows.Forms.TextBox();
            this.textBox_D2 = new System.Windows.Forms.TextBox();
            this.textBox_X1 = new System.Windows.Forms.TextBox();
            this.textBox_D3 = new System.Windows.Forms.TextBox();
            this.textBox_X3 = new System.Windows.Forms.TextBox();
            this.textBox_X2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Ag3 = new System.Windows.Forms.TextBox();
            this.textBox_Ag2 = new System.Windows.Forms.TextBox();
            this.textBox_Ag1 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(428, 299);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(76, 32);
            this.button_cancel.TabIndex = 8;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(523, 299);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(76, 32);
            this.button_OK.TabIndex = 7;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Параметры полигармонического сигнала";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBox_D1);
            this.panel1.Controls.Add(this.textBox_D2);
            this.panel1.Controls.Add(this.textBox_X1);
            this.panel1.Controls.Add(this.textBox_D3);
            this.panel1.Controls.Add(this.textBox_X3);
            this.panel1.Controls.Add(this.textBox_X2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox_Ag3);
            this.panel1.Controls.Add(this.textBox_Ag2);
            this.panel1.Controls.Add(this.textBox_Ag1);
            this.panel1.Location = new System.Drawing.Point(28, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(571, 259);
            this.panel1.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(440, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "Ширина 3 купола";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(440, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Ширина 2 купола";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(200, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Положение центра 3 купола ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(206, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(200, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Положение центра 2 купола ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Амплитуда 3 купола";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Амплитуда 2 купола";
            // 
            // textBox_D1
            // 
            this.textBox_D1.Location = new System.Drawing.Point(443, 58);
            this.textBox_D1.Name = "textBox_D1";
            this.textBox_D1.Size = new System.Drawing.Size(85, 22);
            this.textBox_D1.TabIndex = 13;
            this.textBox_D1.Text = "16";
            this.textBox_D1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_D2
            // 
            this.textBox_D2.Location = new System.Drawing.Point(443, 125);
            this.textBox_D2.Name = "textBox_D2";
            this.textBox_D2.Size = new System.Drawing.Size(85, 22);
            this.textBox_D2.TabIndex = 12;
            this.textBox_D2.Text = "12";
            this.textBox_D2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_X1
            // 
            this.textBox_X1.Location = new System.Drawing.Point(245, 58);
            this.textBox_X1.Name = "textBox_X1";
            this.textBox_X1.Size = new System.Drawing.Size(85, 22);
            this.textBox_X1.TabIndex = 11;
            this.textBox_X1.Text = "101";
            this.textBox_X1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_D3
            // 
            this.textBox_D3.Location = new System.Drawing.Point(443, 197);
            this.textBox_D3.Name = "textBox_D3";
            this.textBox_D3.Size = new System.Drawing.Size(85, 22);
            this.textBox_D3.TabIndex = 10;
            this.textBox_D3.Text = "11";
            this.textBox_D3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_X3
            // 
            this.textBox_X3.Location = new System.Drawing.Point(243, 197);
            this.textBox_X3.Name = "textBox_X3";
            this.textBox_X3.Size = new System.Drawing.Size(85, 22);
            this.textBox_X3.TabIndex = 8;
            this.textBox_X3.Text = "464";
            this.textBox_X3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_X2
            // 
            this.textBox_X2.Location = new System.Drawing.Point(245, 125);
            this.textBox_X2.Name = "textBox_X2";
            this.textBox_X2.Size = new System.Drawing.Size(85, 22);
            this.textBox_X2.TabIndex = 7;
            this.textBox_X2.Text = "302";
            this.textBox_X2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Положение центра 1 купола ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(440, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Ширина 1 купола";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Амплитуда 1 купола";
            // 
            // textBox_Ag3
            // 
            this.textBox_Ag3.Location = new System.Drawing.Point(62, 197);
            this.textBox_Ag3.Name = "textBox_Ag3";
            this.textBox_Ag3.Size = new System.Drawing.Size(85, 22);
            this.textBox_Ag3.TabIndex = 2;
            this.textBox_Ag3.Text = "3";
            this.textBox_Ag3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_Ag2
            // 
            this.textBox_Ag2.Location = new System.Drawing.Point(62, 125);
            this.textBox_Ag2.Name = "textBox_Ag2";
            this.textBox_Ag2.Size = new System.Drawing.Size(85, 22);
            this.textBox_Ag2.TabIndex = 1;
            this.textBox_Ag2.Text = "2";
            this.textBox_Ag2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_Ag1
            // 
            this.textBox_Ag1.Location = new System.Drawing.Point(62, 58);
            this.textBox_Ag1.Name = "textBox_Ag1";
            this.textBox_Ag1.Size = new System.Drawing.Size(85, 22);
            this.textBox_Ag1.TabIndex = 0;
            this.textBox_Ag1.Text = "1";
            this.textBox_Ag1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Gauss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 356);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "Gauss";
            this.Text = "Gauss";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textBox_D1;
        public System.Windows.Forms.TextBox textBox_D2;
        public System.Windows.Forms.TextBox textBox_X1;
        public System.Windows.Forms.TextBox textBox_D3;
        public System.Windows.Forms.TextBox textBox_X3;
        public System.Windows.Forms.TextBox textBox_X2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBox_Ag3;
        public System.Windows.Forms.TextBox textBox_Ag2;
        public System.Windows.Forms.TextBox textBox_Ag1;
    }
}