﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace IT_3
{
    public partial class Form1 : Form
    {
        private Data FormStore;
        public Form1()
        {
            InitializeComponent();
            FormStore = new Data();
        }

        Int16 Len, N, M, P, Num_sob_zn;
        //Len-кол-во отсчётов, N - количество неизвестных, M -количество уравнений, Р-порядок матрицы АКП
        //компоненты для полигармонического сигнала
        Double A1_p, A2_p, A3_p,
                  f1_p, f2_p, f3_p,
                    phase1_p, phase2_p, phase3_p;

        Double[] Poligarmonic_Signal;

        //Компоненты для Гауссова сигнала
        Double A1_g, A2_g, A3_g,
                  D1, D2, D3;

       

        Int16 X1, X2, X3;
        Double[] Gauss_Signal;
        //Компоненты для Экспонециально-затухающего полигармонического сигнала
        Double A1_z, A2_z, A3_z,
                 f1_z, f2_z, f3_z,
                   phase1_z, phase2_z, phase3_z;
        Double[] ExpZ_Signal;


        private void button_parametrs_Click(object sender, EventArgs e)//кнопка отрисовки сигналов
        {
            Len = Int16.Parse(textBox_len.Text);//вытаксиваем значение длины из текстбокса
            ChartSignal.Series[0].Points.Clear();//очищаем чарты
            ChartSignal.Series[1].Points.Clear();
            ChartSignal.Series[2].Points.Clear();
            if (checkBox_Ps.Checked)//Если нажат чекбокс"Полигармонический сигнал", то делаем следующее
            {
                var form = new Poligarmonic();
                var res = form.ShowDialog();//открываем форму, чтобы задать параметры моделируемого сигнала
                if (res == DialogResult.OK)//Если нажали в форме "Ок", то
                {


                    Poligarmonic_Signal = new double[Len];//Задаем величину массива для Полигармонического сигнала

                    A1_p = Convert.ToDouble(form.textBox_Ap1.Text); A2_p = Convert.ToDouble(form.textBox_Ap2.Text); A3_p = Convert.ToDouble(form.textBox_Ap3.Text);//вытаскиваем данные из текстбоксов из формы для параметров
                    f1_p = Convert.ToDouble(form.textBox_f1_p.Text); f2_p = Convert.ToDouble(form.textBox_f2_p.Text); f3_p = Convert.ToDouble(form.textBox_f3_p.Text);
                    phase1_p = Convert.ToDouble(form.textBox_phase1_p.Text); phase2_p = Convert.ToDouble(form.textBox_phase2_p.Text); phase3_p = Convert.ToDouble(form.textBox_phase3_p.Text);
                    Poligarmonic_Signal = FormStore.Gen_Poligarmonic_Signal(Len, A1_p, A2_p, A3_p, f1_p, f2_p, f3_p, phase1_p, phase2_p, phase3_p);//Получаем наш сигнал, записываем данные в соответствующий массив

                    for (int i = 1; i < Len; i++)
                    {
                        ChartSignal.Series[0].Points.AddXY(i, Poligarmonic_Signal[i]);//отрисовываем сигнал
                    }
                }
            }

            if (checkBox_Gauss.Checked)//Если нажат  чекбокс "Гауссовы купола". То делаем - тоже самое, что и с предыдущим чекбоксом,
            {// но уже с другой формой для параметров  и функцией генерируемого сигнала
                var form = new Gauss();
                var res = form.ShowDialog();
                if (res == DialogResult.OK)
                {


                    Gauss_Signal = new double[Len];

                    A1_g = Convert.ToDouble(form.textBox_Ag1.Text); A2_g = Convert.ToDouble(form.textBox_Ag2.Text); A3_g = Convert.ToDouble(form.textBox_Ag3.Text);
                    X1 = Convert.ToInt16(form.textBox_X1.Text); X2 = Convert.ToInt16(form.textBox_X2.Text); X3 = Convert.ToInt16(form.textBox_X3.Text);
                    D1 = Convert.ToDouble(form.textBox_D1.Text); D2 = Convert.ToDouble(form.textBox_D2.Text); D3 = Convert.ToDouble(form.textBox_D3.Text);
                    Gauss_Signal = FormStore.Gen_Gauss_Signal(Len, A1_g, A2_g, A3_g, D1, D2, D3, X1, X2, X3);

                    for (int i = 1; i < Len; i++)
                    {
                        ChartSignal.Series[1].Points.AddXY(i, Gauss_Signal[i]);
                    }

                }


            }

            if (checkBox_ZPs.Checked)//Если нажат чекбокс "Эксп-затух. полигарм.сигнал", все повторяется, опять же другой сигнал
            {
                var form = new ExpZ();
                var res = form.ShowDialog();
                if (res == DialogResult.OK)
                {


                    ExpZ_Signal = new double[Len];

                    A1_z = Convert.ToDouble(form.textBox_Az1.Text); A2_z = Convert.ToDouble(form.textBox_Az2.Text); A3_z = Convert.ToDouble(form.textBox_Az3.Text);
                    f1_z = Convert.ToDouble(form.textBox_f1_z.Text); f2_z = Convert.ToDouble(form.textBox_f2_z.Text); f3_z = Convert.ToDouble(form.textBox_f3_z.Text);
                    phase1_z = Convert.ToDouble(form.textBox_phase1_z.Text); phase2_z = Convert.ToDouble(form.textBox_phase2_z.Text); phase3_z = Convert.ToDouble(form.textBox_phase3_z.Text);
                    ExpZ_Signal = FormStore.Gen_ExpZ_Signal(Len, A1_z, A2_z, A3_z, f1_z, f2_z, f3_z, phase1_z, phase2_z, phase3_z);

                    for (int i = 1; i < Len; i++)
                    {
                        ChartSignal.Series[2].Points.AddXY(i, ExpZ_Signal[i]);
                    }
                }
            }
            ChartSignal.Invalidate();


        }



        private void button_FindSob_Click(object sender, EventArgs e)//кнопка Найти соб.функции и соб.значения
        {
            P = Int16.Parse(textBox_P.Text);//распаковываем текстбокс - получаем порядок АКМ
            Len = Int16.Parse(textBox_len.Text);//количество отсчётов
            Num_sob_zn = Int16.Parse(textBox_num.Text);//Номер собственной функции(вектора)

            chart_SobstvZnach.Series[0].Points.Clear();//очищаем чарты
            chart_SobstvZnach.Series[1].Points.Clear();
            chart_SobstvZnach.Series[2].Points.Clear();
            chart_SobFunc.Series[0].Points.Clear();//очищаем чарты
            chart_SobFunc.Series[1].Points.Clear();
            chart_SobFunc.Series[2].Points.Clear();
            if (checkBox_Ps.Checked)//Если выбран чекбокс "Полигармонический сигнал"
            {
                Double[] U_p = new Double[(P + 1) * (P + 1)]; Double[] V_p = new Double[(P + 1) * (P + 1)];//создаем унитарные матрицы
                Double[] Matr_Sigma_p = new Double[(P + 1) * (P + 1)];//Матрица для хранения собственных чисел
                Double[] AKM_p = new Double[(P + 1) * (P + 1)];//АКМ - сигнала
                for (int i = 0; i < (P + 1) * (P + 1); i++)//забиваем нулями
                {
                    U_p[i] = 0; V_p[i] = 0;
                    Matr_Sigma_p[i] = 0;
                    AKM_p[i] = 0;
                }
                AKM_p = AKM(P, Len, Poligarmonic_Signal);//получаем АКМ
                SVD(P + 1, P + 1, AKM_p, U_p, V_p, Matr_Sigma_p);// подаем все в сингуярное разложение и получаем собственные числа
                for (int i = 0; i < P; i++) chart_SobstvZnach.Series[0].Points.AddXY(i, Matr_Sigma_p[i]);//отрисовка на графике соб.значений
                double[] S_Vector_p = new double[(P + 1) * (P + 1)];//Создаем массив под собственный вектор
                for (int i = 0; i < (P + 1) * (P + 1); i++) S_Vector_p[i] = 0;//опять заполняем нулями

                for (int i = 0; i < P + 1; i++)
                {
                    if (Num_sob_zn > (P))// если номер собственного вектора больше порядка АКП,то 
                    {
                        MessageBox.Show("Не существует заданного собственного вектора!");
                        return;
                    }
                    else S_Vector_p[i] = V_p[i * (P + 1) + Num_sob_zn];//либо столбец правой унитарной матрицы
                }
                for (int i = 0; i < P; i++) chart_SobFunc.Series[0].Points.AddXY(i, S_Vector_p[i]);//отрисовка собственного вектора

            }
            if (checkBox_Gauss.Checked)
            {
                Double[] U_g = new Double[(P + 1) * (P + 1)]; Double[] V_g = new Double[(P + 1) * (P + 1)];//создаем унитарные матрицы
                Double[] Matr_Sigma_g = new Double[(P + 1) * (P + 1)];//Матрица для хранения собственных чисел
                Double[] AKM_g = new Double[(P + 1) * (P + 1)];//АКМ - сигнала
                for (int i = 0; i < (P + 1) * (P + 1); i++)//забиваем нулями
                {
                    U_g[i] = 0; V_g[i] = 0;
                    Matr_Sigma_g[i] = 0;
                    AKM_g[i] = 0;
                }
                AKM_g = AKM(P, Len, Gauss_Signal);//получаем АКМ
                SVD(P + 1, P + 1, AKM_g, U_g, V_g, Matr_Sigma_g);// подаем все в сингуярное разложение и получаем собственные числа
                for (int i = 0; i < P; i++) chart_SobstvZnach.Series[1].Points.AddXY(i, Matr_Sigma_g[i]);//отрисовка на графике соб.значений
                double[] S_Vector_g = new double[(P + 1) * (P + 1)];//Создаем массив под собственный вектор
                for (int i = 0; i < (P + 1) * (P + 1); i++) S_Vector_g[i] = 0;//опять заполняем нулями

                for (int i = 0; i < P + 1; i++)
                {
                    if (Num_sob_zn > (P))// если номер собственного вектора больше порядка АКП,то 
                    {
                        MessageBox.Show("Не существует заданного собственного вектора!");
                        return;
                    }
                    else S_Vector_g[i] = V_g[i * (P + 1) + Num_sob_zn];//либо столбец правой унитарной матрицы
                }
                for (int i = 0; i < P; i++) chart_SobFunc.Series[1].Points.AddXY(i, S_Vector_g[i]);//отрисовка собственного вектора

            }
            if (checkBox_ZPs.Checked)//Если нажат чекбокс "Эксп-затух. полигарм.сигнал", все повторяется, опять же другой сигнал
            {
                Double[] U_e = new Double[(P + 1) * (P + 1)]; Double[] V_e = new Double[(P + 1) * (P + 1)];//создаем унитарные матрицы
                Double[] Matr_Sigma_e = new Double[(P + 1) * (P + 1)];//Матрица для хранения собственных чисел
                Double[] AKM_e = new Double[(P + 1) * (P + 1)];//АКМ - сигнала
                for (int i = 0; i < (P + 1) * (P + 1); i++)//забиваем нулями
                {
                    U_e[i] = 0; V_e[i] = 0;
                    Matr_Sigma_e[i] = 0;
                    AKM_e[i] = 0;
                }
                AKM_e = AKM(P, Len, ExpZ_Signal);//получаем АКМ
                SVD(P + 1, P + 1, AKM_e, U_e, V_e, Matr_Sigma_e);// подаем все в сингуярное разложение и получаем собственные числа
                for (int i = 0; i < P; i++) chart_SobstvZnach.Series[2].Points.AddXY(i, Matr_Sigma_e[i]);//отрисовка на графике соб.значений
                double[] S_Vector_e = new double[(P + 1) * (P + 1)];//Создаем массив под собственный вектор
                for (int i = 0; i < (P + 1) * (P + 1); i++) S_Vector_e[i] = 0;//опять заполняем нулями

                for (int i = 0; i < P + 1; i++)
                {
                    if (Num_sob_zn > (P))// если номер собственного вектора больше порядка АКП,то 
                    {
                        MessageBox.Show("Не существует заданного собственного вектора!");
                        return;
                    }
                    else S_Vector_e[i] = V_e[i * (P + 1) + Num_sob_zn];//либо столбец правой унитарной матрицы
                }
                for (int i = 0; i < P; i++) chart_SobFunc.Series[2].Points.AddXY(i, S_Vector_e[i]);//отрисовка собственного вектора

            }
            chart_SobstvZnach.Invalidate();
            chart_SobFunc.Invalidate();
        }



        int SVD(int P, int K, double[] A, double[] U, double[] V, double[] Sigma)//Сингулярное разложение
        {
            double thr = 1e-4f, nul = 1e-16f;
            int n, m, i, j, l, k, iter, inn, ll, kk;
            bool lort;
            double alfa, betta, hamma, eta, t, cos0, sin0, buf, s;
            n = P;
            m = K;
            for (i = 0; i < n; i++)
            {

                inn = i * n;
                for (j = 0; j < n; j++)
                    if (i == j) V[inn + j] = 1.0;
                    else V[inn + j] = 0.0;
            }
            for (i = 0; i < m; i++)
            {
                inn = i * n;
                for (j = 0; j < n; j++)
                {
                    U[inn + j] = A[inn + j];
                }
            }

            iter = 0;
            while (true)
            {
                lort = false;
                iter++;
                for (l = 0; l < n - 1; l++)
                    for (k = l + 1; k < n; k++)
                    {
                        alfa = 0.0; betta = 0.0; hamma = 0.0;
                        for (i = 0; i < m; i++)
                        {
                            inn = i * n;
                            ll = inn + l;
                            kk = inn + k;
                            alfa += U[ll] * U[ll];
                            betta += U[kk] * U[kk];
                            hamma += U[ll] * U[kk];
                        }

                        if (Math.Sqrt(alfa * betta) < nul) continue;
                        if (Math.Abs(hamma) / Math.Sqrt(alfa * betta) < thr) continue;

                        lort = true;
                        eta = (betta - alfa) / (2f * hamma);
                        t = (double)((eta / Math.Abs(eta)) / (Math.Abs(eta) + Math.Sqrt(1.0 + eta * eta)));
                        cos0 = (double)(1.0 / Math.Sqrt(1.0 + t * t));
                        sin0 = t * cos0;

                        for (i = 0; i < m; i++)
                        {
                            inn = i * n;
                            buf = U[inn + l] * cos0 - U[inn + k] * sin0;
                            U[inn + k] = U[inn + l] * sin0 + U[inn + k] * cos0;
                            U[inn + l] = buf;

                            if (i >= n) continue;
                            buf = V[inn + l] * cos0 - V[inn + k] * sin0;
                            V[inn + k] = V[inn + l] * sin0 + V[inn + k] * cos0;
                            V[inn + l] = buf;
                        }
                    }

                if (!lort) break;
            }

            for (i = 0; i < n; i++)
            {
                s = 0.0;
                for (j = 0; j < m; j++) s += U[j * n + i] * U[j * n + i];
                s = (double)Math.Sqrt(s);
                Sigma[i] = s;
                if (s < nul) continue;
                for (j = 0; j < m; j++) U[j * n + i] /= s;
            }
            //======= Sortirovka ==============
            for (i = 0; i < n - 1; i++)
                for (j = i; j < n; j++)
                    if (Sigma[i] < Sigma[j])
                    {
                        s = Sigma[i]; Sigma[i] = Sigma[j]; Sigma[j] = s;
                        for (k = 0; k < m; k++)
                        { s = U[i + k * n]; U[i + k * n] = U[j + k * n]; U[j + k * n] = s; }
                        for (k = 0; k < n; k++)
                        { s = V[i + k * n]; V[i + k * n] = V[j + k * n]; V[j + k * n] = s; }
                    }

            return iter;
        }

        private Double[] AKM(int P, double Len, double[] Signal)
        {

            double y;
            double[] avk = new double[(P + 1) * (P + 1)];

            for (int i = 0; i < P + 1; i++)
            {
                y = 0;
                for (int j = 0; j < Len - i; j++)
                {
                    y += Signal[j] * Signal[i + j];
                }
                avk[i] = y / (Len + 1 - i);
            }

            for (int i = 0; i < (P + 1); i++)
            {
                for (int j = 0; j < (P + 1); j++)
                {
                    avk[i * (P + 1) + j] = avk[Math.Abs(j - i)];
                }
            }
            return avk;

        }



       private Double[,] Matrix(int n)//создание матрицы размером NxM
        {
            Double[,] A = new Double[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    A[i, j] = ((i == j) ? 1 : 0);
            return A;
        }
        private Double[,] R_Matrix(int n, Double[,] A, Double[,] Ed)//Расширенная матрица
        {
            Double[,] R = new Double[n, 2 * n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 2 * n; j++)
                    R[i, j] = ((j < n) ? A[i, j] : Ed[i, j - n]);

            }
            return R;
        }
        private Double Determinant(Double[,] R, int n)//Ищем детерминант матрицы
        {
            double d = 1;
            for (int i = 0; i < n; i++)
                d *= R[i, i];
            return d;
        }


        private Double[,] Gaus_pryamoy_hod(double[,] R, int n, int m)
        {
            double temp = 0;

            for (int i = 0; i < n; i++)
                for (int j = i + 1; j < n; j++)
                {
                    temp = (R[i, j] / R[i, i]);
                    for (int k = i; k < m; k++)
                    {
                        R[j, k] -= (R[i, k] * temp);
                    }

                }
            return R;
        }




        // private void button_Information_Click(object sender, EventArgs e)//Кнопка "Информация о СЛУ и ее решении"
        private void button1_Click(object sender, EventArgs e)
       
    {
        var form = new Result();//
        
        Double[,] mas;
            
                if (checkBox_chSL.Checked)//Если частный случай, то нам известна матрица, записанная в массиве mas[]
                {
                    Double[] Mass = new Double[9] { 1.01, 2.01, 3.01, 4.01, 5.01, 6.01, 7.01, 8.01, 9.01 };
                    mas = new Double[3, 3] { { 1.01, 2.01, 3.01 }, { 4.01, 5.01, 6.01 }, { 7.01, 8.01, 9.01 } };
                    int[,] triangle = new int[3, 3];

                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            triangle[i, j] = (int)mas[i, j];


                    Double[,] buf = new Double[3, 3];
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            buf[i, j] = mas[i, j];




                    double[,] Pr = Gaus_pryamoy_hod(R_Matrix(3, buf, Matrix(3)), 3, 6);//проходка прямого метода Гаусса


                    textBox_N.Text = Convert.ToString(3);//записываем значения размеров в текстбоксы для данной СЛУ
                    textBox_M.Text = Convert.ToString(3);

                    Double[] U = new Double[9]; Double[] V = new Double[9]; Double[] Sigma = new Double[9];//Создаем унитарные матрицы размера 3х3 и матрицу собственных значений
                    SVD(3, 3, Mass, U, V, Sigma);//Сингулярное разложения
                    Double[,] V_ = new Double[3, 3];
                    Double[,] U_transponirovannaya = new Double[3, 3];
                    Double[,] U_ = new Double[3, 3];
                    Double[,] Sigma_ = new Double[3, 3];
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            V_[i, j] = V[i * 3 + j];//записываем двумерный массив в одномерный - правая матрица собственных векторов                      
                    double det = Determinant(Pr, 3);//вычисляем детерминант

                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            U_[i, j] = U[i * 3 + j];//тоже записываем в одномерный массив - левая матрица собственных векторов

                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            U_transponirovannaya[i, j] = U_[j, i];//траноспонируем матрицу

                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (i == j)
                            {
                                if (Sigma[i] >= 0.1 * Sigma[0])
                                {
                                    Sigma_[i, j] = 1 / Sigma[i];
                                }
                                else Sigma_[i, j] = 0;
                            }

                            else Sigma_[i, j] = 0;
                        }
                    }

                    Double[,] M = new Double[3, 3];
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            for (int k = 0; k < 3; k++)
                                M[i, j] += V_[i, k] * Sigma_[k, j];//умножаем матрицы V и Sigma



                    Double[,] Psevdo_obratnaya_Matrix = new Double[3, 3];//Двумерный массив для Псевдообратной матрицы размером 3х3
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            for (int k = 0; k < 3; k++)
                                Psevdo_obratnaya_Matrix[i, j] += M[i, k] * U_transponirovannaya[k, j];//находим псевдообратную матрицу



                    Random rnd = new Random();//вызываем класс Рандом
                    double min = -1.0, max = 1.0;//минимальное и максимальное значение чисел для интервала рандома
                    Double[] B = new Double[3];

                    for (int i = 0; i < 3; i++)
                        B[i] = rnd.NextDouble() * (max - min) + min;

                    Double[] s = new Double[3]; Double[] P = new Double[3 * 3];
                    int z = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            P[z] = Psevdo_obratnaya_Matrix[i, j];
                            z++;
                        }
                    }

                    for (int i = 0; i < 3; i++)
                    {
                        s[i] = 0.0;

                        for (int j = 0; j < 3; j++)
                        {
                            s[i] += P[i * 3 + j] * B[j];
                        }
                    }

                    // Вычисление невязки:
                    Double E = 0.0;
                    for (int i = 0; i < 3; i++)
                    {
                        Double Sum = 0.0;
                        for (int j = 0; j < 3; j++)
                        {
                            Sum += Mass[i * 3 + j] * s[j];
                        }

                        E += (Sum - B[i]) * (Sum - B[i]) / 3;
                    }


                    //***************************Выводим инфу в текстбоксы*************************************************/
                    form.textBox_Nevayzky.Text = Convert.ToString(String.Format("{0:F4}", E));
                    form.textBox_det.Text = Convert.ToString(String.Format("{0:F7}", det));
                    StringBuilder S1 = new StringBuilder(); StringBuilder S2 = new StringBuilder();
                    StringBuilder S3 = new StringBuilder(); StringBuilder S4 = new StringBuilder();
                    StringBuilder S5 = new StringBuilder(); StringBuilder S6 = new StringBuilder();
                    StringBuilder S7 = new StringBuilder(); StringBuilder S8 = new StringBuilder();
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            S1.Append(mas[i, j] + "   ");
                        }
                        S1.AppendLine("\n");
                    }
                    form.textBox_IshodMatr.TextAlign = HorizontalAlignment.Center;
                    form.textBox_IshodMatr.Text = S1.ToString();//Записывем в текст бокс исходный матрицу

                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            S2.Append(Math.Round(V_[i, j], 3) + "   ");
                        }
                        S2.AppendLine("\n");
                    }
                    form.textBox_MatrLS.TextAlign = HorizontalAlignment.Center;
                    form.textBox_MatrLS.Text = S2.ToString();//Записывем в текстбокс левую матрицу собственных векторов


                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            S3.Append(Math.Round(U_[i, j], 3) + "   ");
                        }
                        S3.AppendLine("\n");
                    }
                    form.textBox_MprS.TextAlign = HorizontalAlignment.Center;
                    form.textBox_MprS.Text = S3.ToString();//Записывем в текстбокс правую матрицу собственных векторов


                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            S4.Append(Math.Round(U_transponirovannaya[i, j], 3) + "   ");
                        }
                        S4.AppendLine("\n");
                    }
                    form.textBox_transpMatrL.TextAlign = HorizontalAlignment.Center;
                    form.textBox_transpMatrL.Text = S4.ToString();//Записывем в текстбокс правую матрицу собственных векторов




                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            S5.Append(Math.Round(Sigma_[i, j], 3) + "   ");
                        }
                        S5.AppendLine("\n" + "   ");
                    }
                    form.textBox_SZ.TextAlign = HorizontalAlignment.Center;
                    form.textBox_SZ.Text = S5.ToString();//Записывем в текстбокс матрицу соб.значений в степени -1.



                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            S6.Append(Math.Round(Psevdo_obratnaya_Matrix[i, j], 3) + "   ");
                        }
                        S6.AppendLine("\n");
                    }
                    form.textBox_PsObr.TextAlign = HorizontalAlignment.Center;
                    form.textBox_PsObr.Text = S6.ToString();//Записывем в текстбокс  псевдообратную  матрицу 


                    for (int i = 0; i < 3; i++)
                        S7.AppendLine(Math.Round(s[i], 4) + "\n");
                    S7.AppendLine("\n");
                    form.textBox_Neizv.TextAlign = HorizontalAlignment.Center;
                    form.textBox_Neizv.Text = S7.ToString();//Записывем в текстбокс  псевдообратную  матрицу 

                    for (int i = 0; i < 3; i++)
                        S8.AppendLine(Math.Round(B[i], 4) + "\n");
                    form.textBox_svobod.TextAlign = HorizontalAlignment.Center;
                    form.textBox_svobod.Text = S8.ToString();//записываем в текстбокс векстор свободных членов


                }
                else
                {

                    N = Convert.ToInt16(textBox_N.Text);
                    M = Convert.ToInt16(textBox_M.Text);

                    if (N == M)
                    {
                        mas = new double[N, M];
                        Random rnd = new Random();
                        double[] Mas = new double[N * M];
                        for (int i = 0; i < N * M; i++)
                        {
                            Mas[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                        }
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                mas[i, j] = Mas[i * N + j];

                        int[,] triangle = new int[N, M];
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                triangle[i, j] = (int)mas[i, j];

                        Double[,] buf = new Double[N, M];
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                buf[i, j] = mas[i, j];
                        Double[,] Pryamoy = Gaus_pryamoy_hod(R_Matrix(N, buf, Matrix(N)), N, 2 * M);


                        Double[] U = new Double[N * M]; Double[] V = new Double[N * M];
                        Double[] Sigma = new Double[N * M];
                        Double det = Determinant(Pryamoy, N);
                        SVD(N, M, Mas, U, V, Sigma);
                        Double[,] V_ = new Double[N, M]; Double[,] U_ = new Double[N, M];
                        Double[,] U_transponirovannaya = new Double[N, M];

                        Double[,] Sigma_ = new Double[N, M];
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                V_[i, j] = V[i * 3 + j];

                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                U_[i, j] = U[i * 3 + j];
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                U_transponirovannaya[i, j] = U_[j, i];



                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                if (i == j)
                                {
                                    if (Sigma[i] >= 0.1 * Sigma[0])
                                    {
                                        Sigma_[i, j] = 1 / Sigma[i];
                                    }
                                    else Sigma_[i, j] = 0;
                                }

                                else Sigma_[i, j] = 0;
                            }
                        }
                        Double[,] Matr = new Double[N, M];
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                for (int k = 0; k < N; k++)
                                    Matr[i, j] += V_[i, k] * Sigma_[k, j];

                        Double[,] Psevdo_obratnaya_Matrix = new Double[N, M];
                        for (int i = 0; i < N; i++)
                            for (int j = 0; j < M; j++)
                                for (int k = 0; k < N; k++)
                                    Psevdo_obratnaya_Matrix[i, j] += Matr[i, k] * U_transponirovannaya[k, j];
                        Double[] B = new Double[N];

                        for (int i = 0; i < N; i++)
                            B[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;


                        Double[] S = new Double[N];
                        Double[] Ps = new Double[N * M];
                        int z = 0;
                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                Ps[z] = Psevdo_obratnaya_Matrix[i, j];
                                z++;
                            }
                        }

                        for (int i = 0; i < N; i++)
                        {
                            S[i] = 0.0;

                            for (int j = 0; j < N; j++)
                            {
                                S[i] += Ps[i * N + j] * B[j];
                            }


                        }

                        // Вычисление невязки:
                        Double E = 0.0;
                        for (int i = 0; i < N; i++)
                        {
                            Double M_ = 0.0;
                            for (int j = 0; j < N; j++)
                            {
                                M_ += Mas[i * N + j] * S[j];
                            }

                            E += (M_ - B[i]) * (M_ - B[i]) / N;
                        }

                        form.textBox_Nevayzky.Text = Convert.ToString(String.Format("{0:F4}", E));
                        form.textBox_det.Text = Convert.ToString(String.Format("{0:F7}", det));
                        StringBuilder S1 = new StringBuilder(); StringBuilder S2 = new StringBuilder();
                        StringBuilder S3 = new StringBuilder(); StringBuilder S4 = new StringBuilder();
                        StringBuilder S5 = new StringBuilder(); StringBuilder S6 = new StringBuilder();
                        StringBuilder S7 = new StringBuilder(); StringBuilder S8 = new StringBuilder();
                        //
                        StringBuilder sw = new StringBuilder();

                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                S1.Append(Math.Round(mas[i, j], 2) + "   ");
                            }
                            S1.AppendLine("\n");
                        }
                        form.textBox_IshodMatr.TextAlign = HorizontalAlignment.Center;
                        form.textBox_IshodMatr.Text = S1.ToString();//Записывем в текст бокс исходный матрицу

                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                S2.Append(Math.Round(V_[i, j], 3) + "   ");
                            }
                            S2.AppendLine("\n");
                        }
                        form.textBox_MatrLS.TextAlign = HorizontalAlignment.Center;
                        form.textBox_MatrLS.Text = S2.ToString();//Записывем в текстбокс левую матрицу собственных векторов



                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                S3.Append(Math.Round(U_[i, j], 3) + "   ");
                            }
                            S3.AppendLine("\n");
                        }
                        form.textBox_MprS.TextAlign = HorizontalAlignment.Center;
                        form.textBox_MprS.Text = S3.ToString();//Записывем в текстбокс правую матрицу собственных векторов

                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                S4.Append(Math.Round(U_transponirovannaya[i, j], 3) + "   ");
                            }
                            S4.AppendLine("\n");
                        }
                        form.textBox_transpMatrL.TextAlign = HorizontalAlignment.Center;
                        form.textBox_transpMatrL.Text = S4.ToString();//Записывем в текстбокс правую матрицу собственных векторов



                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                S5.Append(Math.Round(Sigma_[i, j], 3) + "   ");
                            }
                            S5.AppendLine("\n" + "   ");
                        }
                        form.textBox_SZ.TextAlign = HorizontalAlignment.Center;
                        form.textBox_SZ.Text = S5.ToString();//Записывем в текстбокс матрицу соб.значений в степени -1.


                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < M; j++)
                            {
                                S6.Append(Math.Round(Psevdo_obratnaya_Matrix[i, j], 3) + "   ");
                            }
                            S6.AppendLine("\n");
                        }
                        form.textBox_PsObr.TextAlign = HorizontalAlignment.Center;
                        form.textBox_PsObr.Text = S6.ToString();//Записывем в текстбокс  псевдообратную  матрицу 

                        for (int i = 0; i < N; i++)
                            S7.AppendLine(Math.Round(S[i], 4) + "\n");
                        S7.AppendLine("\n");
                        form.textBox_Neizv.TextAlign = HorizontalAlignment.Center;
                        form.textBox_Neizv.Text = S7.ToString();//Записывем в текстбокс  псевдообратную  матрицу 
                        for (int i = 0; i < N; i++)
                            S8.AppendLine(Math.Round(B[i], 4) + "\n");
                        form.textBox_svobod.TextAlign = HorizontalAlignment.Center;
                        form.textBox_svobod.Text = S8.ToString();//записываем в текстбокс векстор свободных членов

                    }

                }
            var res = form.ShowDialog();//открываем форму
        }


    }
}
